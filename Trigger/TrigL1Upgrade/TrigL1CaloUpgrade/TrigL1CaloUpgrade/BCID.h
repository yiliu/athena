/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/


#ifndef TRIGL1CALOUPGRADE_BCID
#define TRIGL1CALOUPGRADE_BCID

const int first_bcid[]={1,81,161,241,321};

int bcids_from_start(const int& bcid);

#endif
