# Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration

from AthenaCommon.CfgGetter import addTool
addTool("HGTDG4SD.HGTDG4SDConfig.getHGTDSensorSD"       , "HGTDSensorSD" )
